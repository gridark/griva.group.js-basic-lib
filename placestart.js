/**
 * PS Basic Library
 *
 * @author  Vasily GRIDARK Grigoriev
 * @company Place-Start
 *
 * @description Данная библиотека содержит код, позволяющий упроситить доступ к данным, элементам DOM, а так же некоторые другие функции и плагины
 *
 * @version 1.2.7
 */


/**
 * Глобальная переменная
 */
if( typeof ps == "undefined" ) var ps = {};

/**
 * Если данная библиотека была подключена ранее, то выдаем предупреждение
 * @since 1.2.4
 * @type {String}
 */
ps.reused = typeof ps.version != "undefined" ? '#reuse_warning['+ps.version+']' : '';

/**
 * Переменная, указывающая номер, дату и время создания версии
 * @since 1.2.1
 * @type {String}
 */
ps.version = '1.2.7@20.06.17/09:47' + ps.reused;

/**
 * Данный объект может содержать короткие ссылки на функции
 * @since 1.2.0
 * @type {Object}
 */
ps.sh = ps.sh || {};

(function (jQuery) {
    /**
     * Функция добавляет возможность сравнения двух массивов
     * @since 1.2.6
     * @see    http://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript
     * @param  {array}   array Массив, с которым нежно сравнить исходный
     * @return {boolean}       Результат проверки
     */
    Array.prototype.equals = function (array)
    {
        // Если массив принимает значение, приводимые к логическому false,
        // то завершаем проверку с отрицательным исходом
        if (!array) return false;
        // Если наши масивы по размеру разные - завершаем проверку
        // В источнике запись: Экономит много времени
        if (this.length != array.length) return false;

        for (var i = 0, l=this.length; i < l; i++) {
            // Если элементы массива - вложенные массивы, то устраиваем рекурсию
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].equals(array[i])) return false;
            } else if (this[i] != array[i]) {
                // Если элементы не равны - выходим с отрицанием
                return false;
            }
        }
        return true;
    }
    // Скрываем метод в for-in циклах
    Object.defineProperty(Array.prototype, "equals", {enumerable: false});
    
    /**
     * Регистрируем функцию на событие, привязанное к элементу <body>
     * Очень полезен для динамических элементов, которые обновляются с помощью ajax
     * @see  jQuery.fn.on();
     */
    jQuery.fn.life = function (types, data, fn)
    {
        jQuery('body').on( types, this.selector, data, fn);
        return this;
    };

    /**
     * Снимает выполнение функции с события, привязанного к элементу <body>
     * @see  jQuery.fn.off();
     */
    jQuery.fn.death = function (types)
    {
        jQuery('body').off(types, this.selector);
        return this;
    };

    /**
     * Функция проверяет наличие атрибута у объекта DOM
     * @since 1.2.3
     * @param  {string}  name Полное название атрибута
     * @return {boolean}      Результат проверки
     */
    jQuery.fn.hasAttr = function (name)
    {
        return (typeof this.attr(name) !== 'undefined' && this.attr( name ) !== false);
    };

    /**
     * Удаляет все классы эелемента по его маске
     * @param  {string} mask Маска класса, где * - любое кол-во символов
     * @return {object}      Возвращает jQuery объект
     */
    jQuery.fn.removeClassMask = function (mask)
    {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };

    /**
     * Функция возвращает данные формы в виде одномерного объекта ключ => значение
     * @since 1.2.4
     * @return {Object}
     */
    jQuery.fn.formToObject = function ()
    {
        var raw_data, data = {};
        raw_data = this.serialize()
        raw_data.split("&").forEach(function(part) {
            var item = part.split("=");
            data[item[0]] = decodeURIComponent(item[1]);
        });
        return data;
    }

    /**
     * Данный код позволяет назначить любому элементу функции ссылки, добавив атрибут data-href, содержащий ссылку на конечную цель
     * @since 1.2.1
     */
    jQuery(document).on( "click", "[data-href]", function(event)
    {
        event.preventDefault();
        window.location = jQuery( this ).attr( "data-href" );
    });
    
    /**
     * Функция, которая получает данные из строки запроса по ключу
     * @param  {string}      search_key Ключ поиска
     * @return {string|bool}            Значение из строки запроса либо false, если ключ не найден
     */
    ps._GET = function (search_key)
    {
        var _GET    = window.location.search.substring( 1 ).split( "&" );
        var __GET   = {};

        for( var i=0; i<_GET.length; i++ ) { 
            var param = _GET[i].split( "=" );
            __GET[ param[ 0 ] ] = param[ 1 ];
        }       

        if( typeof search_key == "string" ) {
            if( typeof __GET[ search_key ] != "undefined" ) return __GET[ search_key ];
            else return false;
        } else return __GET;
    };

    /**
     * Функция предоставляет доступ к управлению данными cookie
     * @since 1.2.2
     * @param  {mixed} method {object} Если передан объект и в нем существует параметр name, то вызывается функция cookie.set()
     *                        {string} Если передана строка, то вызывается метод функции (если существует)
     *                                 Если передана строка и метода с данным названием нет, то вызывается функция cookie.get()
     * @return {mixed}        Вернет ответ метода
     */
    ps._COOKIE = function(method)
    {
        var cookie = {};

        /**
         * Функция получает значение cookie по имени. Если такой печеньки не существует, то возвращается undefined
         * @param  {mixed} search_key {string} Имя cookie
         *                            {object} Массив имен cookie
         * @return {mixed}            Значение найденного cookie либо undefined
         */
        cookie.get = function( search_key ) {
            if( typeof search_key == 'undefined' ) return undefined;
            if( typeof search_key == 'object' ) {
                var results = {};
                for( var key in search_key ) {
                    results[ search_key[ key ] ] = cookie.get( search_key[ key ] );
                }
                return results;
            } else {
                var matches = document.cookie.match( new RegExp( "(?:^|; )" + search_key.replace( /([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1' ) + "=([^;]*)" ));
                return matches ? decodeURIComponent(matches[1]) : undefined;
            }
        }

        /**
         * Функция может записывать значение cookie
         * @param  {mixed}   name    {string} Имя cookie
         *                           {object} Полное описание cookie в виде объекта
         * @param  {mixed}   value   {string} Новое значение cookie
         *                           {null} Передается null, если необходимо удалить cookie
         * @param  {object}  options Параметры cookie
         * @return {boolean}         Результат выполнения функции
         */
        cookie.set = function( name, value, options ) {
            // Мы должны получать только строку или объект первым аргументом
            if( typeof name != 'object' && typeof name != 'string' ) return false;

            value = value || null;
            options = options || {};

            // Если к нам приешл объект - разбираем его
            if( typeof name == 'object' ) {
                var _arguments = name;

                if( typeof _arguments.name == 'undefined' ) return false;

                name = _arguments.name;
                value = _arguments.value || null;
                options = _arguments.options || {};
            }

            // Функция удаления cookie
            if( value === null ) options.expires = -1;  

            options = cookie._parse_args( options );
            options = cookie._format_args( options );

            value = encodeURIComponent( value );

            var _cookie = name + "=" + value;
            document.cookie = _cookie + options;

            return cookie.get( name ) == decodeURIComponent( value );
        }

        /**
         * Функция удаляет cookie по его имени
         * @param  {string}  name Имя cookie
         * @return {boolean}      Результат удаления
         */
        cookie.unset = function( name ) {
            cookie.set( name, null );
            return undefined == cookie.get( name );
        }

        /**
         * Вспомогательная функция парсинга параметров cookie
         * @param  {object} options Параметры cookie
         * @return {object}         Формализованные параметры cookie
         */
        cookie._parse_args = function( options ) {
            var args = {};
            var default_args = {
                'domain': null,
                'path': '/',
                'expires': null,
                'secure' : false,
            };

            if( typeof options !== 'object' ) args = default_args;
            else {
                for( var key in default_args ) {
                    args[ key ] = typeof options[ key ] != 'undefined' ? options[ key ] : default_args[ key ];
                }
            }

            if( ! ( args.expires instanceof Date ) && args.expires !== null ) {
                if( typeof args.expires == 'number' ) args.expires = new Date( new Date().getTime() + ( args.expires * 1000 ) );
                else args.expires = null;
            }

            return args;
        }

        /**
         * Вспомогательная функция форматирования параметров cookie
         * @param  {object} options Параметры cookie
         * @return {string}         Параметры cookie в виде строки
         */
        cookie._format_args = function( options ) {
            return ([
                ( typeof options.expires === 'object' && options.expires instanceof Date ? '; expires=' + options.expires.toGMTString() : '' ),
                ( '; path=' + options.path),
                ( typeof options.domain === 'string' ? '; domain=' + options.domain : '' ),
                ( options.secure === true ? '; secure' : '' ),
            ].join(''));
        }

        if( typeof cookie[ method ] == 'function' ) return cookie[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ) );
        else if( typeof method == 'object' && typeof method.name != 'undefined' ) return cookie.set.apply( this, arguments );
        else return cookie.get.apply( this, arguments );
    };

    /**
     * Функция предоставляет функционал для реализации флагов состояния
     * @since 1.2.5
     * @return {Object}
     */
    ps.locks = (function(){
        var locks = {};

        return {
            /**
             * Функция устанавливает новый флаг с идентификатором {lockId}
             * @param {String} lockId Уникальный идентификатор флага
             */
            set : function(lockId)
            {
                if( typeof lockId == "string" ) {
                    locks[lockId] = true;
                    return true;
                } else {
                    console.error('The flag ID must be a string [ps.locks.set()]', lockId);
                    return false;
                }
            },
            /**
             * Функция устанавливает флаг с идентификатором {lockId} в положение false
             * @param {String} lockId Уникальный идентификатор флага
             */
            unset : function(lockId)
            {
                if( typeof lockId == "string" ) {
                    locks[lockId] = false;
                    return true;
                } else {
                    console.error('The flag ID must be a string [ps.locks.unset()]', lockId);
                    return false;
                }
            },
            /**
             * Функция очищает все состояния флагов, путем их удаления из локального хранилища
             */
            unsetAll : function()
            {
                locks = {};
            },
            /**
             * Функция получает статус флага по его идентификатору
             * @param  {String}  lockId Уникальный идентификатор флага
             * @return {Boolean}        Результат поиска
             */
            get : function(lockId)
            {
                if( typeof lockId == "string" && typeof locks[lockId] == "boolean" ) {
                    return locks[lockId];
                } else {
                    return false;
                }
            }
        }
    })();

    /**
     * Функция предоставляет функционал для реализации стека данных
     * @since 1.2.5
     * @return {Object}
     */
    ps.queue = (function(){ return {
        list : {},

        /**
         * Функция добавляет или перезаписывает новое значение в поток
         * @param {String}  queueId       Уникальный идентификатор потока
         * @param {Mixed}   queueData     Данныен включаемые в поток
         * @param {Boolean} reuseInstance Режим записи в поток (true - перезаписывать|false - добавить)
         */
        set : function(queueId, queueData, reuseInstance)
        {
            if (typeof reuseInstance == 'undefined') reuseInstance = false;
            if (typeof this.list[queueId] == "undefined" || reuseInstance) this.list[queueId] = [];
            this.list[queueId].push(queueData);
        },

        /**
         * Функция позволяет обнулять значение потока
         * @param  {String} queueId    Уникальный идентификатор потока
         * @return {Void}
         */
        unset : function(queueId) {
            if(typeof this.list[queueId] !== 'undefined')
            this.list[queueId] = [];
        },

        /**
         * Функция получает необходимые данные из потока.
         * @param  {String}  queueId     Уникальный идентификатор потока
         * @param  {Boolean} removeAfter Удалять ли занчение после получения
         * @return {Mixed}               Значение потока
         */
        get : function(queueId, removeAfter) {
            if( typeof removeAfter == "undefined" ) removeAfter = true;
            if( typeof this.list[queueId] == "undefined" || this.list[queueId].length < 1 ) return null;
            if( removeAfter == true ) return this.list[queueId].shift();
            else return this.list[queueId].slice(0, 1)[0];
        }
    }})();

    /**
     * Функция позволяет создавать древовидный стек с группами
     * @since 1.2.5
     * @return {Object}
     */
    ps.queue.groups = (function(){return {
        list : {},

        /**
         * Функция позволяет создавать древовидные потоки и добавлять в них значения
         * @param {String}  queueGroup    Уникальный идентификатор группы потока
         * @param {String}  queueId       Уникальный идентификатор потока
         * @param {Mixed}   queueData     Данныен включаемые в поток
         * @param {Boolean} reuseInstance Режим записи в поток (true - перезаписывать|false - добавить)
         */
        set : function(queueGroup, queueId, queueData, reuseInstance)
        {
            if (typeof reuseInstance == 'undefined') reuseInstance = false;
            if (typeof this.list[queueGroup] == "undefined") this.list[queueGroup] = [];

            var offsetId = false;
            for (var i = this.list[queueGroup].length - 1; i >= 0; i--) {
                if( this.list[queueGroup][i].id == queueId ) offsetId = i;
            }

            if(offsetId === false) {
                this.list[queueGroup].push({ id : queueId, queue : [] });
                offsetId = this.list[queueGroup].length - 1;
            }

            if (reuseInstance) this.list[queueGroup][offsetId].queue = [];
            this.list[queueGroup][offsetId].queue.push(queueData);
        },

        /**
         * Позволяет обнулять группы потоков, либо один из группы
         * @param  {String} queueGroup Уникальный идентификатор группы потока
         * @param  {String} queueType  Уникальный идентификатор потока
         * @return {Void}
         */
        unset : function(queueGroup, queueType)
        {
            var unsetType = 'group';
            if (typeof queueId != 'undefined') {
                unsetType = 'id';
            }

            if (unsetType == 'group' && typeof this.list[queueGroup] !== 'undefined') {
                this.list[queueGroup] = [];
            } else if (unsetType == 'id' && typeof this.list[queueGroup][queueId] !== 'undefined') {
                this.list[queueGroup][queueId] = [];
            }
        },

        /**
         * Функция получает текущее значение одного потока в группе
         * @param  {String}  queueGroup  Уникальный идентификатор группы потока
         * @param  {Boolean} removeAfter Удалять значение после получения значения?
         * @return {Mixed}               Значение потока, либо {null} при их отсутсвии
         */
        get : function(queueGroup, removeAfter) {
            if(typeof this.list[queueGroup] == 'undefined') return null;
            if( typeof removeAfter == "undefined" ) removeAfter = true;
            return this._getQueue( queueGroup, removeAfter );
        },

        /**
         * Рекурсивная функция для посика текущего значения потока
         * @param  {String}  queueGroup  Уникальный идентификатор группы потока
         * @param  {Boolean} removeAfter Удалять значение после получения значения?
         * @return {Mixed}               Значение потока, либо {null} при их отсутсвии
         */
        _getQueue : function(queueGroup, removeAfter)
        {
            if (this.list[queueGroup].length < 1) return null;
            var currentQueueLength = this.list[queueGroup].slice(0, 1)[0].queue.length;

            if (currentQueueLength > 0) {
                if( removeAfter == true ) return this.list[queueGroup].slice(0, 1)[0].queue.shift();
                else return this.list[queueGroup].slice(0, 1)[0].queue.slice(0, 1)[0];
            } else {
                this.list[queueGroup].shift();
                return this._getQueue(queueGroup, removeAfter);
            }
        }
    }})();

    /**
     * Обработчик для тригеров
     */
    ps.action = function ( name_action, custom_prefix, vars ) {
        if( typeof name_action == "undefined" ) return false;
        if( typeof custom_prefix == "undefined" || typeof custom_prefix != "string" ) var prefix = "ps"; else var prefix = custom_prefix;
        
        if( typeof name_action == "string" ) {
            name_action = name_action.replace( /\,\s/g, ',' );
            name_action = name_action.split(",");
        }

        if( typeof name_action == "object" ) {
            for( id in name_action ) jQuery( window ).trigger( prefix + "_" + name_action[ id ], vars );
            return true;
        } else return false;

    };

    /**
     * Функция позволяет проверять сущевствование методов, функций и переменных
     * и при желании выводить форматированное сообщение об ошибке
     * 
     * @param  {string|array} check_object  Имя проверяемого объекта. Если необходимо проверить сущевствование
     *                                      внутри объекта (метода/свойства) - передается массив, в котором
     *                                      первым элементом передается объект, а во второй передается имя проверяемого объекта.
     * @param  {bool}         error_message Выводить ошибку в консоль? По умолчанию - false
     * @return {bool}                       Результат проверки
     */
    ps.exists = function( check_object, error_message ) {
        if( typeof error_message == 'undefined' ) error_message = false;

        var result = false;

        if( typeof check_object == 'object' ) {
            result = typeof check_object[0][ check_object[1] ] != 'undefined';
        } else {
            result = typeof window[check_object] != 'undefined';
        }

        if( ! result && error_message ) console.error( error_message );
        return result;
    };
    
    /**
     * Глобальная переменная для модуля хуков
     * @since 1.2.0
     * @type {Object}
     */
    ps.hooks = {};

    /**
     * Список зарегистрированных хуков
     * @since 1.2.0
     * @type {Object}
     */
    ps.hooks.list = {};

    /**
     * Регистрация новго хука
     * @param {string}   hook_name ID хука, к которому применяется функция {action}
     * @param function action    Функция, применяемая к набору данных при вызове хука
     */
    ps.hooks.add = function( hook_name, action ) {
        if( 'undefined' == typeof ps.hooks.list[hook_name] ) ps.hooks.list[hook_name] = [];
        ps.hooks.list[hook_name].push( action );
    };

    /**
     * Вызов хука по его ID
     * @param  {string} hook_name ID хука
     * @param  {mixed}  data      Данные, которые необходимо обработать
     * @return {mixed}            Обработанные данные
     */
    ps.hooks.use = function( hook_name, data ) {
        if( ps.hooks.list.length < 1 || 'undefined' == typeof ps.hooks.list[hook_name] ) return data;
        for( var method_id in ps.hooks.list[hook_name] ) {
            data = ps.hooks.list[hook_name][method_id].apply( null, Array.prototype.slice.call( arguments, 1 ) );
        }

        return data;
    };

    /**
     * Данный блок кода служит для поддержки обратной совместимости некторых улучшенных функций
     */
    
    ps.locks.func = {};

    /**
     * Функция добавляет или перезаписывает новое значение в поток
     * @deprecated 1.2.5
     * @see ps.queue.set();
     * @param {string}  queueId       Уникальный идентификатор потока
     * @param {mixed}   queueData     Данныен включаемые в поток
     * @param {boolean} reuseInstance Режим записи в поток (true - перезаписывать|false - добавить)
     */
    ps.locks.func.add_queue = function(queueId, queueData, reuseInstance)
    {
        ps._deprecated('ps.locks.func.add_queue', '1.2.5', '1.5.0', 'ps.queue.set');
        return ps.queue.set(queueId, queueData, reuseInstance)
    };

    /**
     * Функция получает необходимые данные из потока.
     * @deprecated 1.2.5
     * @see ps.queue.get();
     * @param  {string}  queueId     Уникальный идентификатор потока
     * @param  {boolean} removeAfter Удалять ли занчение после получения
     * @return {mixed}               Значение потока
     */
    ps.locks.func.get_queue = function(queueId, removeAfter)
    {
        ps._deprecated('ps.locks.func.get_queue', '1.2.5', '1.5.0', 'ps.queue.get');
        return ps.queue.get(queueId, removeAfter)
    };

    /**
     * Функция получает статус флага по его идентификатору
     * @deprecated 1.2.5
     * @see ps.locks.get();
     * @param  {string}  lockId Уникальный идентификатор флага
     * @return {boolean}        Результат поиска
     */
    ps.locks.func.check = function(lockId)
    {
        ps._deprecated('ps.locks.func.check', '1.2.5', '1.5.0', 'ps.locks.get');
        return ps.locks.get(lockId)
    };

    /**
     * Функция устанавливает новый флаг с идентификатором {lockId}
     * @deprecated 1.2.5
     * @see ps.locks.get();
     * @param {string} lockId Уникальный идентификатор флага
     */
    ps.locks.func.set = function(lockId)
    {
        ps._deprecated('ps.locks.func.set', '1.2.5', '1.5.0', 'ps.locks.set');
        return ps.locks.set(lockId)
    };

    /**
     * Функция устанавливает флаг с идентификатором {lockId} в положение false
     * @deprecated 1.2.5
     * @see ps.locks.unset();
     * @param {string} lockId Уникальный идентификатор флага
     */
    ps.locks.func.del = function(lockId)
    {
        ps._deprecated('ps.locks.func.del', '1.2.5', '1.5.0', 'ps.locks.unset');
        return ps.locks.unset(lockId)
    };

    /**
     * Функция выводит предупреждение о скором удалении функции и возможного переноса ее функциональности в другие
     * @since 1.2.7
     * @param  {string}         functionName  Название функции, признанной устаревшей
     * @param  {string}         fromVersion   С какой версии считается устаревшей в формате d.d.d
     * @param  {string|boolean} removeVersion В какой версии функция будет удалена в формате d.d.d,
     *                                        если неизвестно, то можно задать false, что повлечет пропуск фразы
     * @param  {string|boolean} migrateTo     Название функции, которая позволит ее заменить функционально,
     *                                        если не предусмотрена замена - false, что бы пропустить фразу
     * @return {null}
     */
    ps._deprecated = function (functionName, fromVersion, removeVersion = false, migrateTo = false)
    {
        var warnString = 'Function {'+functionName+'} is deprecated from version '+fromVersion+'!';
        if (removeVersion) {
            warnString = warnString + ' In the version '+removeVersion+' it will be removed.';
        }
        if (migrateTo) {
            warnString = warnString + ' Please use the function {'+migrateTo+'}.';
        }
        console.warn(warnString);
    }
})(jQuery);
