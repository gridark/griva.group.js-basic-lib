# PS Basic Library

Данная библиотка содержит несколько функций-помошников, которые предоставляют удобный доступ и управление данными страницы. Зависит только от наличия подключенного jQuery.

## API
Global variable
---------------
Объявляется глобальная переменная, которая доступна в любой момент времени. Для защиты от перезаписи вводится проверка на сущевствование.
```javascript
if( typeof ps == "undefined" ) var ps = {};
```
Versions
-----------
Создана переменная, в которой содержится номер, дата и время создания текущей версии
Шаблон создания расширенного номера версии: `[Номер версии в формате d.d.d]@[Дата создания версии в формате dd.mm.yy]/[Время создания версии в формате hh:mm]`. Если библиотека подключается второй раз, то к номеру версии добавляется строка `#reuse_warning[версия уже подключенной библиотеки]`
```javascript
/**
 * Переменная, указывающая номер, дату и время создания версии
 * @since 1.2.1
 * @type {String}
 */
ps.version = '1.2.6@25.04.17/10:50' + ps.reused;
```
Array equal array
---------------------------
Функция добавляет возможность сравнения двух массивов
```javascript
/**
 * Функция добавляет возможность сравнения двух массивов
 * @since 1.2.6
 * @see    http://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript
 * @param  {array}   array Массив, с которым нежно сравнить исходный
 * @return {boolean}       Результат проверки
 */
Array.prototype.equals = function (array)
{
    // Если массив принимает значение, приводимые к логическому false,
    // то завершаем проверку с отрицательным исходом
    if (!array) return false;
    // Если наши масивы по размеру разные - завершаем проверку
    // В источнике запись: Экономит много времени
    if (this.length != array.length) return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Если элементы массива - вложенные массивы, то устраиваем рекурсию
        if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].equals(array[i])) return false;
        } else if (this[i] != array[i]) {
            // Если элементы не равны - выходим с отрицанием
            return false;
        }
    }
    return true;
}
// Скрываем метод в for-in циклах
Object.defineProperty(Array.prototype, "equals", {enumerable: false});
```
#### Пример:
```javascript
[1, 2, [3, 4]].equals([1, 2, [3, 2]]) === false;
[1, "2,3"].equals([1, 2, 3]) === false;
[1, 2, [3, 4]].equals([1, 2, [3, 4]]) === true;
[1, 2, 1, 2].equals([1, 2, 1, 2]) === true;
```
\[jQuery] Additional events
---------------------------
Данные функции предоставляют возможность регистрировать/удалять события на родительский элемент, не заморачиваясь о его классах.
```javascript
jQuery.fn.life = function ( types, data, fn ) {
    jQuery( this.context ).on( types, this.selector, data, fn );
    return this;
};
```
```javascript
jQuery.fn.death = function ( types ) {
    jQuery( this.context ).off( types, this.selector );
    return this;
};
```
\[jQuery] Element has attribute?
---------------------------
Функция выполяет только одно действие - проверяет наличие у текущего объекта атрибута с именем, переданным в функцию
```javascript
/**
 * Функция проверяет наличие атрибута у объекта DOM
 * @since 1.2.3
 * @param  {string}  name Полное название атрибута
 * @return {boolean}      Результат проверки
 */
jQuery.fn.hasAttr = function( name ) {  
    return ( typeof this.attr( name ) !== 'undefined' && this.attr( name ) !== false );
};
```
\[jQuery] Remove class by mask
------------------------------
Фукнция предоставляет возможность удалять классы у элемента по маске
```javascript
jQuery.fn.removeClassMask = function(mask) {
    return this.removeClass(function(index, cls) {
        var re = mask.replace(/\*/g, '\\S+');
        return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
    });
};
```
#### Пример:
```html
<div class="element remove-active remove-current"></div>
<script>jQuery( '.element' ).removeClassMask( "remove-*" );</script>
```
#### Результат:
```html
<div class="element"></div>
<script>jQuery( '.element' ).removeClassMask( "remove-*" );</script>
```
\[jQuery] Get `<form>` data as `{Object}`
------------------------------
Функция возвращает данные формы в виде одномерного объекта ```ключ => значение```
```javascript
jQuery.fn.formToObject = function() {
    var raw_data, data = {};
    raw_data = this.serialize()
    raw_data.split("&").forEach(function(part) {
        var item = part.split("=");
        data[item[0]] = decodeURIComponent(item[1]);
    });
    return data;
}
```
#### Пример:
```html
<form>
    <input name="product_id" value="1731" type="hidden">
    <input name="product_quantity" value="1" type="hidden">
    <input id="_wpnonce" name="_wpnonce" value="5bc3fde469" type="hidden">
    <button>Отправить</button>
</form>
<script>jQuery( 'form' ).formToObject();</script>
```
#### Результат:
```
Object { product_id: "1731", product_quantity: "1", _wpnonce: "5bc3fde469" }
```
[jQuery] Link everywhere
-----------
Данный код позволяет назначать любому элементу страницы функции ссылки
```javascript
jQuery( document ).on( "click", "[data-href]", function( event ) {
    event.preventDefault();
    window.location = jQuery( this ).attr( "data-href" );
});
```
#### Пример:
```html
<button data-href="https://ya.ru">Хочу в Яндекс</button>
```
Get "_GET" parameters
---------------------
Позволяет получить данные из GET запроса
```javascript
ps._GET = function( search_key ) {
    var _GET    = window.location.search.substring( 1 ).split( "&" );
    var __GET   = {};
    ... 
    if( typeof search_key == "string" ) {
        if( typeof __GET[ search_key ] != "undefined" ) return __GET[ search_key ];
        else return false;
    } else return __GET;
}
```
#### Пример:
```
REQUEST: example.com/?register=on&error=no
```
```javascript
console.log( 'error: ' + ps._GET( 'error' ) );
```
#### Результат:
```javascript
error: no
```
Cookie operations
---------------------
Функция и ее методы позволяют управлять cookies
```javascript
/**
 * Функция предоставляет доступ к управлению данными cookie
 * @since 1.2.2
 * @param  {mixed} method {object} Если передан объект и в нем существует параметр name, то вызывается функция cookie.set()
 *                        {string} Если передана строка, то вызывается метод функции (если существует)
 *                                 Если передана строка и метода с данным названием нет, то вызывается функция cookie.get()
 * @return {mixed}        Вернет ответ метода
 */
ps._COOKIE = function( method ) {
    var cookie = {};
    
    /**
     * Функция получает значение cookie по имени. Если такой печеньки не существует, то возвращается undefined
     * @param  {mixed} search_key {string} Имя cookie
     *                            {object} Массив имен cookie
     * @return {mixed}            Значение найденного cookie либо undefined
     */
    cookie.get = function( search_key ) {
        if( typeof search_key == 'undefined' ) return undefined;
        if( typeof search_key == 'object' ) {
            var results = {};
            for( var key in search_key ) {
                results[ search_key[ key ] ] = cookie.get( search_key[ key ] );
            }
            return results;
        } else {
            var matches = document.cookie.match( new RegExp( "(?:^|; )" + search_key.replace( /([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1' ) + "=([^;]*)" ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    /**
     * Функция может записывать значение cookie
     * @param  {mixed}   name    {string} Имя cookie
     *                           {object} Полное описание cookie в виде объекта
     * @param  {mixed}   value   {string} Новое значение cookie
     *                           {null} Передается null, если необходимо удалить cookie
     * @param  {object}  options Параметры cookie
     * @return {boolean}         Результат выполнения функции
     */
    cookie.set = function( name, value, options ) {
        // Мы должны получать только строку или объект первым аргументом
        if( typeof name != 'object' && typeof name != 'string' ) return false;

        value = value || null;
        options = options || {};

        // Если к нам приешл объект - разбираем его
        if( typeof name == 'object' ) {
            var _arguments = name;

            if( typeof _arguments.name == 'undefined' ) return false;

            name = _arguments.name;
            value = _arguments.value || null;
            options = _arguments.options || {};
        }

        // Функция удаления cookie
        if( value === null ) options.expires = -1;  

        options = cookie._parse_args( options );
        options = cookie._format_args( options );

        value = encodeURIComponent( value );

        var _cookie = name + "=" + value;
        document.cookie = _cookie + options;

        return cookie.get( name ) == decodeURIComponent( value );
    }

    /**
     * Функция удаляет cookie по его имени
     * @param  {string}  name Имя cookie
     * @return {boolean}      Результат удаления
     */
    cookie.unset = function( name ) {
        cookie.set( name, null );
        return undefined == cookie.get( name );
    }

    /**
     * Вспомогательная функция парсинга параметров cookie
     * @param  {object} options Параметры cookie
     * @return {object}         Формализованные параметры cookie
     */
    cookie._parse_args = function( options ) {
        var args = {};
        var default_args = {
            'domain': null,
            'path': '/',
            'expires': null,
            'secure' : false,
        };

        if( typeof options !== 'object' ) args = default_args;
        else {
            for( var key in default_args ) {
                args[ key ] = typeof options[ key ] != 'undefined' ? options[ key ] : default_args[ key ];
            }
        }

        if( ! ( args.expires instanceof Date ) && args.expires !== null ) {
            if( typeof args.expires == 'number' ) args.expires = new Date( new Date().getTime() + ( args.expires * 1000 ) );
            else args.expires = null;
        }

        return args;
    }

    /**
     * Вспомогательная функция форматирования параметров cookie
     * @param  {object} options Параметры cookie
     * @return {string}         Параметры cookie в виде строки
     */
    cookie._format_args = function( options ) {
        return ([
            ( typeof options.expires === 'object' && options.expires instanceof Date ? '; expires=' + options.expires.toGMTString() : '' ),
            ( '; path=' + options.path),
            ( typeof options.domain === 'string' ? '; domain=' + options.domain : '' ),
            ( options.secure === true ? '; secure' : '' ),
        ].join(''));
    }

    if( typeof cookie[ method ] == 'function' ) return cookie[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ) );
    else if( typeof method == 'object' && typeof method.name != 'undefined' ) return cookie.set.apply( this, arguments );
    else return cookie.get.apply( this, arguments );
};
```
#### Примеры использования:
Получение cookie по имени или массиву имен
```javascript
ps._COOKIE( 'get', '_ym_uid' );
//"147677655651936282"
ps._COOKIE( '_ym_uid' );
//"147677655651936282"
ps._COOKIE( 'get', [ '_ym_uid', 'wordpress_test_cookie' ] );
//Object { _ym_uid: "147677655651936282", wordpress_test_cookie: "WP+Cookie+check" }
ps._COOKIE( [ '_ym_uid', 'wordpress_test_cookie' ] );
//Object { _ym_uid: "147677655651936282", wordpress_test_cookie: "WP+Cookie+check" }
```
Установка или обновление cookie по имени
```javascript
ps._COOKIE( 'set', 'test', 'PS Cookie Check', { expires: 100 } ); // true
ps._COOKIE({
    name:    'test',
    value:   'PS Cookie Check',
    options: { expires: 100 }
}); // true
```
Удаление cookie
```javascript
ps._COOKIE( 'unset', 'test' );
```
Locks
-------------
Данный функционал предосталяет возможность использования флагов блокировки
```javascript
/**
 * Функция предоставляет функционал для реализации флагов состояния
 * @since 1.2.5
 * @return {Object}
 */
ps.locks = (function(){
    var locks = {};

    return {
        /**
         * Функция устанавливает новый флаг с идентификатором {lockId}
         * @param {String} lockId Уникальный идентификатор флага
         */
        set : function(lockId)
        {
            if( typeof lockId == "string" ) {
                locks[lockId] = true;
                return true;
            } else {
                console.error('ID флага обязан быть строкой [ps.locks.set()]', lockId);
                return false;
            }
        },
        /**
         * Функция устанавливает флаг с идентификатором {lockId} в положение false
         * @param {String} lockId Уникальный идентификатор флага
         */
        unset : function(lockId)
        {
            if( typeof lockId == "string" ) {
                locks[lockId] = false;
                return true;
            } else {
                console.error('ID флага обязан быть строкой [ps.locks.unset()]', lockId);
                return false;
            }
        },
        /**
         * Функция очищает все состояния флагов, путем их удаления из локального хранилища
         */
        unsetAll : function()
        {
            locks = {};
        },
        /**
         * Функция получает статус флага по его идентификатору
         * @param  {String}  lockId Уникальный идентификатор флага
         * @return {Boolean}        Результат поиска
         */
        get : function(lockId)
        {
            if( typeof lockId == "string" && typeof locks[lockId] == "boolean" ) {
                return locks[lockId];
            } else {
                console.error('ID флага обязан быть строкой [ps.locks.get()]', lockId);
                return false;
            }
        }
    }
})();
```
#### Пример:
```javascript
// Устанавливаем флаг
ps.locks.set('request_runing');
// Проверям наличие флага
if (ps.locks.get('request_runing')) {
    console.log('Выполнится, потому что флаг в позиции true');
}
// Удаляем флаг
ps.locks.unset('request_runing');
```
Queue
-----
Данный функционал добавляет импровизированный стэк данных
```javascript
/**
 * Функция предоставляет функционал для реализации стека данных
 * @since 1.2.5
 * @return {Object}
 */
ps.queue = (function(){ return {
    list : {},

    /**
     * Функция добавляет или перезаписывает новое значение в поток
     * @param {String}  queueId       Уникальный идентификатор потока
     * @param {Mixed}   queueData     Данныен включаемые в поток
     * @param {Boolean} reuseInstance Режим записи в поток (true - перезаписывать|false - добавить)
     */
    set : function(queueId, queueData, reuseInstance)
    {
        if (typeof reuseInstance == 'undefined') reuseInstance = false;
        if (typeof this.list[queueId] == "undefined" || reuseInstance) this.list[queueId] = [];
        this.list[queueId].push(queueData);
    },

    /**
     * Функция позволяет обнулять значение потока
     * @param  {String} queueId    Уникальный идентификатор потока
     * @return {Void}
     */
    unset : function(queueId) {
        if(typeof this.list[queueId] !== 'undefined')
        this.list[queueId] = [];
    },

    /**
     * Функция получает необходимые данные из потока.
     * @param  {String}  queueId     Уникальный идентификатор потока
     * @param  {Boolean} removeAfter Удалять ли занчение после получения
     * @return {Mixed}               Значение потока
     */
    get : function(queueId, removeAfter) {
        if( typeof removeAfter == "undefined" ) removeAfter = true;
        if( typeof this.list[queueId] == "undefined" || this.list[queueId].length < 1 ) return null;
        if( removeAfter == true ) return this.list[queueId].shift();
        else return this.list[queueId].slice(0, 1)[0];
    }
}})();
```
#### Пример:
```javascript
// Устанавливаем значение потока
ps.queue.set('request_runing', 'yes', true);
// Получаем значение потока
console.log(ps.queue.get('request_runing', true);
// Удаляем поток
ps.queue.unset('request_runing');
// Так можно посмотреть, что сейчас находится в потоке
console.log(ps.queue.list);
```
Groups Queues
-------------
```javascript
/**
 * Функция позволяет создавать древовидный стек с группами
 * @since 1.2.5
 * @return {Object}
 */
ps.queue.groups = (function(){return {
    list : {},

    /**
     * Функция позволяет создавать древовидные потоки и добавлять в них значения
     * @param {String}  queueGroup    Уникальный идентификатор группы потока
     * @param {String}  queueId       Уникальный идентификатор потока
     * @param {Mixed}   queueData     Данныен включаемые в поток
     * @param {Boolean} reuseInstance Режим записи в поток (true - перезаписывать|false - добавить)
     */
    set : function(queueGroup, queueId, queueData, reuseInstance)
    {
        if (typeof reuseInstance == 'undefined') reuseInstance = false;
        if (typeof this.list[queueGroup] == "undefined") this.list[queueGroup] = [];

        var offsetId = false;
        for (var i = this.list[queueGroup].length - 1; i >= 0; i--) {
            if( this.list[queueGroup][i].id == queueId ) offsetId = i;
        }

        if(offsetId === false) {
            this.list[queueGroup].push({ id : queueId, queue : [] });
            offsetId = this.list[queueGroup].length - 1;
        }

        if (reuseInstance) this.list[queueGroup][offsetId].queue = [];
        this.list[queueGroup][offsetId].queue.push(queueData);
    },

    /**
     * Позволяет обнулять группы потоков, либо один из группы
     * @param  {String} queueGroup Уникальный идентификатор группы потока
     * @param  {String} queueType  Уникальный идентификатор потока
     * @return {Void}
     */
    unset : function(queueGroup, queueType)
    {
        var unsetType = 'group';
        if (typeof queueId != 'undefined') {
            unsetType = 'id';
        }

        if (unsetType == 'group' && typeof this.list[queueGroup] !== 'undefined') {
            this.list[queueGroup] = [];
        } else if (unsetType == 'id' && typeof this.list[queueGroup][queueId] !== 'undefined') {
            this.list[queueGroup][queueId] = [];
        }
    },

    /**
     * Функция получает текущее значение одного потока в группе
     * @param  {String}  queueGroup  Уникальный идентификатор группы потока
     * @param  {Boolean} removeAfter Удалять значение после получения значения?
     * @return {Mixed}               Значение потока, либо {null} при их отсутсвии
     */
    get : function(queueGroup, removeAfter) {
        if(typeof this.list[queueGroup] == 'undefined') return null;
        if( typeof removeAfter == "undefined" ) removeAfter = true;
        return this._getQueue( queueGroup, removeAfter );
    },

    /**
     * Рекурсивная функция для посика текущего значения потока
     * @param  {String}  queueGroup  Уникальный идентификатор группы потока
     * @param  {Boolean} removeAfter Удалять значение после получения значения?
     * @return {Mixed}               Значение потока, либо {null} при их отсутсвии
     */
    _getQueue : function(queueGroup, removeAfter)
    {
        if (this.list[queueGroup].length < 1) return null;
        var currentQueueLength = this.list[queueGroup].slice(0, 1)[0].queue.length;

        if (currentQueueLength > 0) {
            if( removeAfter == true ) return this.list[queueGroup].slice(0, 1)[0].queue.shift();
            else return this.list[queueGroup].slice(0, 1)[0].queue.slice(0, 1)[0];
        } else {
            this.list[queueGroup].shift();
            return this._getQueue(queueGroup, removeAfter);
        }
    }
}})();
```
#### Пример:
```javascript
// Устанавливаем значение потока
ps.queue.groups.set('request_group', 'request_runing', 'yes', true);
// Получаем значение потока
console.log(ps.queue.groups.get('request_group', true);
// Удаляем удаляем группу потоков
ps.queue.groups.unset('request_group');
// Удаляем удаляем необходимы поток в группе
ps.queue.groups.unset('request_group', 'request_runing');
// Так можно посмотреть, что сейчас находится в потоке
console.log(ps.queue.groups.list);
```
\[jQuery] Do many triggers
--------------------------
Предосталяет возможность добавлять хуки в независимые бибилотеки с минимальным изменением их исходного кода.
```javascript
ps.action = function ( name_action, custom_prefix, vars ) {
    if( typeof name_action == "undefined" ) return false;
    if( typeof custom_prefix == "undefined" || typeof custom_prefix != "string" ) var prefix = "ps"; else var prefix = custom_prefix;
    
    if( typeof name_action == "string" ) {
        name_action = name_action.replace( /\,\s/g, ',' );
        name_action = name_action.split(",");
    }

    if( typeof name_action == "object" ) {
        for( id in name_action ) jQuery( window ).trigger( prefix + "_" + name_action[ id ], vars );
        return true;
    } else return false;

}
```
Method/Function/Variable exists?
--------------------------------
Следующая функция позволяет проверять сущевствование методов, функций и переменных и при желании выводить форматированное сообщение об ошибке
```javascript
/**
 * Функция позволяет проверять сущевствование методов, функций и переменных
 * и при желании выводить форматированное сообщение об ошибке
 * 
 * @param  {string|array} check_object  Имя проверяемого объекта. Если необходимо проверить сущевствование
 *                                      внутри объекта (метода/свойства) - передается массив, в котором
 *                                      первым элементом передается объект, а во второй передается имя проверяемого объекта.
 * @param  {bool}         error_message Выводить ошибку в консоль? По умолчанию - false
 * @return {bool}                       Результат проверки
 */
ps.exists = function( check_object, error_message ) {
    if( typeof error_message == 'undefined' ) error_message = false;

    var result = false;

    if( typeof check_object == 'object' ) {
        result = typeof check_object[0][ check_object[1] ] != 'undefined';
    } else {
        result = typeof window[check_object] != 'undefined';
    }

    if( ! result && error_message ) console.error( error_message );
    return result;
};
```
Hooks for JS
------------
Данный модуль добавляет возможность использования хуков в JS-коде
```javascript
/**
 * Глобальная переменная для модуля хуков
 * @since 1.2.0
 * @type {Object}
 */
ps.hooks = {};

/**
 * Список зарегистрированных хуков
 * @since 1.2.0
 * @type {Object}
 */
ps.hooks.list = {};

/**
 * Регистрация новго хука
 * @param {string}   hook_name ID хука, к которому применяется функция {action}
 * @param function action    Функция, применяемая к набору данных при вызове хука
 */
ps.hooks.add = function( hook_name, action ) {
    if( 'undefined' == typeof ps.hooks.list[hook_name] ) ps.hooks.list[hook_name] = [];
    ps.hooks.list[hook_name].push( action );
};

/**
 * Вызов хука по его ID
 * @param  {string} hook_name ID хука
 * @param  {mixed}  data      Данные, которые необходимо обработать
 * @return {mixed}            Обработанные данные
 */
ps.hooks.use = function( hook_name, data ) {
    if( ps.hooks.list.length < 1 || 'undefined' == typeof ps.hooks.list[hook_name] ) return data;
    for( var method_id in ps.hooks.list[hook_name] ) {
        data = ps.hooks.list[hook_name][method_id].apply( null, Array.prototype.slice.call( arguments, 1 ) );
    }

    return data;
};
```
#### Пример:
```javascript
// Проверяем, заданны ли действия при хуке 'ps/geography--object-manager--settings'
// Данная конструкция if( ps.hooks.use( ... , false ) === false ) позволяет выполнить
// все заданные действия, либо, при их отсутствии, выполнить другой произвольный код
if( ps.hooks.use( 'ps/geography--object-manager--settings', false ) === false ) {
    ps.geography.yandex.object_manager.objects.options.set('preset', 'islands#redCircleDotIcon');
    ps.geography.yandex.object_manager.clusters.options.set('preset', 'islands#redClusterIcons');
}
```
Short links
-----------
Специальное пространство имен, куда можно записывать частоиспользуемые функции, пренебрегая их названием
```javascript
/**
 * Данный объект может содержать короткие ссылки на функции
 * @since 1.2.0
 * @type {Object}
 */
ps.sh = ps.sh || {};
```
#### Пример:
```javascript
/**
 * Данная функция предназначена для гибкого изменения настроек ymaps без изменения плагина
 * @param  {string} settings_name ID настройки
 * @return {mixed}
 */
ps.sh.go_gs = function( settings_name ) {
    if( 'undefined' == typeof settings_name || 'undefined' == typeof ps.geography.yandex.settings[settings_name] ) return undefined;
    var prefix = 'ps/geography--';
    var args = Array.prototype.slice.call(arguments, 1);
        args.unshift( prefix + settings_name, ps.geography.yandex.settings[settings_name] );
    return ps.hooks.use.apply( null, args );
}
```
It is DEPRECATED!!!!
-----------
Функция собирает фразу о том, что функция `functionName` является устаревшей и выводит в консоль.
```javascript
/**
 * Функция выводит предупреждение о скором удалении функции и возможного переноса ее функциональности в другие
 * @since 1.2.7
 * @param  {string}         functionName  Название функции, признанной устаревшей
 * @param  {string}         fromVersion   С какой версии считается устаревшей в формате d.d.d
 * @param  {string|boolean} removeVersion В какой версии функция будет удалена в формате d.d.d,
 *                                        если неизвестно, то можно задать false, что повлечет пропуск фразы
 * @param  {string|boolean} migrateTo     Название функции, которая позволит ее заменить функционально,
 *                                        если не предусмотрена замена - false, что бы пропустить фразу
 * @return {null}
 */
ps._deprecated = function (functionName, fromVersion, removeVersion = false, migrateTo = false)
{
    var warnString = 'Function {'+functionName+'} is deprecated from version '+fromVersion+'!';
    if (removeVersion) {
        warnString = warnString + ' In the version '+removeVersion+' it will be removed.';
    }
    if (migrateTo) {
        warnString = warnString + ' Please use the function {'+migrateTo+'}.';
    }
    console.warn(warnString);
}
```
